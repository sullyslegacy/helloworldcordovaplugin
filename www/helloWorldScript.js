/*
	Alternative to cordova.exec();
	var exec = require('cordova/exec');
	//exec(successCallback, errorCallback, "FirstPlugin", "helloWorld", []);		
*/

var helloWorld = {
	sayHelloWorld: function(successCallback, errorCallback){
		try
		{
			cordova.exec(successCallback, errorCallback, "HelloWorldPlugin", "helloWorld", []);
		}
		catch(error)
		{
			alert(error);
		}
	},
	sayHello: function(successCallback, errorCallback, name){
		try
		{
			cordova.exec(successCallback, errorCallback, "HelloWorldPlugin", "sayHello", [name]);
		}
		catch(error)
		{
			alert(error);
		}
	},
	killBackButton: function(){
		// Override back button action to prevent being killed
		document.addEventListener('backbutton', function () {}, false);
	}
}

//This is done in clobber via window.helloWorld
module.exports = helloWorld;