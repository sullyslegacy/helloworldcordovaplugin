package com.sullyslegacy;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HelloWorldPlugin extends CordovaPlugin {

	
	//The plugin should handle pause and resume events, and any message passing between plugins
	// Plugins with long-running requests, background activity such as media playback, listeners, or internal state should implement the onReset()
	
	@Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("sayHello")) {
            String message = args.getString(0);
            sayHello(message, callbackContext);
            return true;
        }
        else {
            helloWorld(callbackContext);
            return true;
        }
        //return false;
    }
	
	
	private void helloWorld(CallbackContext callbackContext) {
        callbackContext.success("Hello World");
	}
	
	private void sayHello(String message, CallbackContext callbackContext) {
	    if (message != null && message.length() > 0) {
	        callbackContext.success(message);
	    } 
	    else {
	        callbackContext.error("Expected one non-empty string argument.");
	    }
	}
	 	
}
